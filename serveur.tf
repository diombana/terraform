terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.22.0"
    }
  }
}


provider "aws"{

    region = "${var.region}"
    access_key = "${var.cle_acces}"
    secret_key = "${var.cle_secrete}"
}

resource "aws_vpc" "vpc_tp" {
  cidr_block = "${var.cidr_vpc}"
  tags = {
    Name = "${var.nom_vpc}"
  }
  
}
data "aws_availability_zones" "TouteZones"{
  state = "available"
}
resource "aws_subnet" "public" {
  count = 2
  vpc_id = aws_vpc.vpc_tp.id
  cidr_block = cidrsubnet(var.cidr_block_public, 2, count.index+1)
  availability_zone = data.aws_availability_zones.TouteZones.names[count.index+1]
  map_public_ip_on_launch = true
  tags = {
    Name = "Makan_Subnet-public ${count.index+1}"
  }
}
resource "aws_subnet" "private" {
  count = 2
  vpc_id     = aws_vpc.vpc_tp.id  
  cidr_block = cidrsubnet(var.cidr_block_private, 2, count.index+1)
  availability_zone = data.aws_availability_zones.TouteZones.names[count.index+1]
  map_public_ip_on_launch = false
  tags = {
    Name = "Makan_Subnet-private ${count.index+1}"
  }
}

resource "aws_internet_gateway" "igw"{
  vpc_id = aws_vpc.vpc_tp.id
  tags = {
    Name = "Makan_public-gateway"
  }
} 
resource "aws_eip" "ip-ngw" {
  count = 2
  vpc      = true
  tags = {
    Name = "Makan_elastic ip ${count.index+1}"
  }
}
resource "aws_nat_gateway" "ngw"{
  count = 2
  allocation_id = aws_eip.ip-ngw[count.index].id
  subnet_id = aws_subnet.public[count.index].id

}
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.vpc_tp.id
  count = 2
  
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = "Makan_public-rt ${count.index+1}"
  }
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.vpc_tp.id
  count = 2
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.ngw[count.index].id
  }
  tags = {
    Name = "Makan_private-rt ${count.index+1}"
  }
}
resource "aws_route_table_association" "public" {
  count = 2
  subnet_id = aws_subnet.public[count.index].id 
  route_table_id = aws_route_table.public[count.index].id

}
 resource "aws_route_table_association" "private" {
  count = 2
  subnet_id = aws_subnet.private[count.index].id 
  route_table_id = aws_route_table.private[count.index].id

}
resource "aws_security_group" "GrpTp"{
  vpc_id = aws_vpc.vpc_tp.id
  name = "${var.nom_groupe_securite}"
  ingress{
    from_port =  22
    to_port =  22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress{
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    Name = "SG_Makan"
  }


}
