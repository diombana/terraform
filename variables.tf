variable "region" {
    default = "us-east-1"
}
variable "cle_acces" {
    default = "AKIA2V5XXBBAELABQGGX"
}
variable "cle_secrete" {
    default = "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"
}
variable "my_ami"{
    default = "ami-0960ab670c8bb45f3"
}
variable "key_name" {
    default = "key_of_makan"
}
variable "instance_type" {
    default = "t2.micro"
}

variable "nom_instance" {
    default = "Server_Of_Makan"
}
variable "nom_vpc" {
    default = "VPC_OF_MAKAN"
}
variable "cidr_vpc" {
    default = "10.0.0.0/16"
}
variable "nom_groupe_securite" {
    default = "SG_OF_Makan"

}

variable "cidr_block_public" {
    default = "10.0.1.0/24" 
}
variable "cidr_block_private" {
    default = "10.0.2.0/24" 
}

